//! commands for uploading code to screeps
#![allow(clippy::tabs_in_doc_comments)]
mod new;
mod upload;
use clap::{Parser, Subcommand};
use std::env;

#[derive(Parser)]
#[command(about, version, bin_name = "cargo screeps")]
struct Args {
	#[command(subcommand)]
	command: Command,
}
#[derive(Subcommand)]
enum Command {
	/// puts default template files in directory
	New {
		/// the directory to put new files in, or uses current
		directory: Option<String>,
	},
	/// uploads files from directory to screeps
	Upload {
		/// branch to upload to, if different from config file
		#[arg(short, long)]
		branch: Option<String>,
		/// make a new branch in screeps before uploading
		#[arg(short, long)]
		new: bool,
		/// configuration file to look for in the directory, defaults to config.toml
		#[arg(short, long)]
		config: Option<String>,
		/// the directory to upload, or uses current
		directory: Option<String>,
	},
}

fn main() {
	//remove cargo's duplicate command thing
	let mut args: Vec<String> = env::args().collect();
	if let Some(second) = args.get(1) {
		if args.first().unwrap().ends_with(second) {
			args.remove(1);
		}
	}
	// dbg!(args.clone());
	let args = Args::parse_from(args.iter());

	match args.command {
		Command::New { directory } => new::new(&directory.unwrap_or(".".to_string())),
		Command::Upload {
			branch,
			new,
			config,
			directory,
		} => upload::upload(
			branch,
			new,
			&config.unwrap_or("config.toml".to_string()),
			&directory.unwrap_or(".".to_string()),
		),
	}
}
