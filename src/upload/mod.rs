mod build;
mod modules;
mod rest;
use self::rest::{Authentication, Server};
use serde::{Deserialize, Serialize};
use std::{
	fs::File,
	io::{self, Write},
	process,
};

#[derive(Deserialize, Serialize)]
struct Config {
	server: Server,
	branch: Option<String>,
}
impl Config {
	fn read(path: &str) -> io::Result<Config> {
		let data = std::fs::read_to_string(path)?;
		Ok(toml::from_str(&data).expect("valid config file"))
	}
	fn official() -> Self {
		Self {
			server: Server::new(
				"https://screeps.com".to_string(),
				443,
				Authentication::Token("token".to_string()),
			),
			branch: Some("default".to_string()),
		}
	}
	fn private() -> Self {
		Self {
			server: Server::new(
				"url here".to_string(),
				21025,
				Authentication::UsernamePassword("username".to_string(), "password".to_string()),
			),
			branch: Some("default".to_string()),
		}
	}
	fn write(&self, path: &str) {
		let config = toml::to_string(self).expect("valid default config");
		let mut file = File::create(path).expect("permission to create file");
		file
			.write_all(config.as_bytes())
			.expect("permission to write to file")
	}
}

fn prompt(text: &str) -> String {
	println!("{text}");
	let mut input = String::new();
	io::stdin().read_line(&mut input).expect("reading stdin");
	input
}
pub fn upload(branch: Option<String>, new: bool, config: &str, directory: &str) {
	println!("uploading {directory} with {config}");
	let config = &format!("{directory}/{config}");
	let Ok(config) = Config::read(config) else {
		let consent = prompt(&format!(
			"issue reading {config} config file. Make default file? (y/N)"
		));
		if consent.trim().to_lowercase() == "y" {
			let default_type = prompt("make config file for official server or private server? (O/p)");
			let default_config = if default_type.trim().to_lowercase() == "p" {
				Config::private()
			} else {
				Config::official()
			};
			default_config.write(config);
			println!("made {config} config file, please fill it in and run again");
		}
		process::exit(0);
	};
	let branch = branch.unwrap_or(config.branch.unwrap_or("something".to_string()));
	println!("to branch {branch}");
	if new {
		println!("creating new branch {branch}");
		config.server.new_branch(&branch);
	}

	build::wasm(directory, "wasm", "wasm");

	let mut data = modules::Data::new(&branch);
	data.read(&format!("{directory}/src"));
	data.read(&format!("{directory}/wasm"));

	config.server.push_modules(&data);
}

#[cfg(test)]
mod tests {
	#[test]
	fn upload() { super::upload(Some("test".to_string()), true, "config.toml", "./test") }
}
