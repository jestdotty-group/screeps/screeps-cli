use base64::{engine::general_purpose::STANDARD_NO_PAD, Engine};
use serde::Serialize;
use serde_json::Value;
use std::{collections::HashMap, fs};

#[derive(Serialize)]
pub struct Data {
	branch: String,
	modules: HashMap<String, Value>,
}
impl Data {
	pub fn new(branch: &str) -> Self {
		Self {
			branch: branch.to_string(),
			modules: HashMap::new(),
		}
	}
	fn read_recursively(&mut self, root: &str, location: Vec<&str>) {
		let paths = fs::read_dir(
			[root]
				.into_iter()
				.chain(location.clone())
				.collect::<Vec<_>>()
				.join("/"),
		)
		.expect("permission to read directory")
		.filter_map(|r| r.ok())
		.map(|d| d.path());

		for path in paths {
			//FileType should be an enum but alas, rust.
			if path.is_file() {
				let Some(extension) = path.extension() else { continue };
				let Some(name) = path.file_stem() else { continue };
				let Some(extension) = extension.to_str() else { continue };
				let Some(name) = name.to_str() else { continue };
				self.modules.insert(
					location
						.clone()
						.into_iter()
						.chain([name])
						.collect::<Vec<_>>()
						.join("/"),
					match extension {
						"js" =>
							serde_json::Value::String(fs::read_to_string(&path).expect("permission to read")),
						"wasm" => serde_json::json!({
							"binary": STANDARD_NO_PAD.encode(fs::read(&path).expect("permission to read")),
						}),
						_ => continue,
					},
				);
			} else if path.is_dir() {
				let Some(path) = path.file_stem() else { continue };
				let Some(path) = path.to_str() else { continue };
				let mut location = location.clone();
				location.push(path);
				self.read_recursively(root, location);
			}
		}
	}
	pub fn read(&mut self, root: &str) { self.read_recursively(root, Vec::new()) }
}
#[cfg(test)]
mod tests {
	use crate::upload::build;
	#[test]
	fn data() {
		let mut data = super::Data::new("test");
		data.read("./test/src");
		build::wasm("./test", "wasm", "wasm");
		data.read("./test/wasm");
		dbg!(data.modules.keys());
		data.modules.get("main").expect("main.js file");
		data.modules.get("wasm_bg").expect("wasm_bg.wasm file");
		data.modules.get("wasm").expect("wasm.js file");
	}
}
