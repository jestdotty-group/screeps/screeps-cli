use super::modules;
use reqwest::blocking::RequestBuilder;
use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize)]
pub struct Server {
	url: String,
	port: u16,
	authentication: Authentication,
}
#[derive(Deserialize, Serialize)]
#[serde(untagged)] // changes it from "Token" to just one string or two strings!
pub enum Authentication {
	Token(String),
	UsernamePassword(String, String),
}
impl Server {
	pub fn new(url: String, port: u16, authentication: Authentication) -> Self {
		Self {
			url,
			port,
			authentication,
		}
	}
	/// creates a request to the server including the authentication
	fn call(&self, path: &str) -> RequestBuilder {
		let client = reqwest::blocking::Client::new();
		let request = client.post(format!("{}:{}/api/{path}", self.url, self.port));
		match &self.authentication {
			Authentication::Token(token) => request.header("X-Token", token),
			Authentication::UsernamePassword(username, password) =>
				request.basic_auth(username, Some(password)),
		}
	}
	/// makes a new branch using api/user/clone-branch, which is how screeps does it
	pub fn new_branch(&self, branch: &str) {
		#[derive(Serialize)]
		#[allow(non_snake_case)]
		struct Data {
			branch: String,
			newName: String,
		}
		let response = self
			.call("user/clone-branch")
			.json(&Data {
				branch: branch.to_string(),
				newName: branch.to_string(),
			})
			.send()
			.unwrap();

		let status = response.status();
		let url = response.url().clone();
		let text = response.text().unwrap();
		println!("{status:4}: {url}\n{text}");
	}
	/// pushes to given branch the Modules given
	pub fn push_modules(&self, data: &modules::Data) {
		let response = self.call("user/code").json(data).send().unwrap();
		let status = response.status();
		let url = response.url().clone();
		let text = response.text().unwrap();
		println!("{status:4}: {url}\n{text}");
	}
}
