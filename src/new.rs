use std::fs;

pub fn new(directory: &str) {
	fs::create_dir_all(format!("{directory}/src")).expect("creating directory permissions");
	fs::write(
		format!("{directory}/src/main.js"),
		include_str!("../test/src/main.js"),
	)
	.expect("writing main.js");
	fs::write(
		format!("{directory}/src/js.rs"),
		include_str!("../test/src/js.rs"),
	)
	.expect("writing js.rs");
	fs::write(
		format!("{directory}/src/lib.rs"),
		include_str!("../test/src/lib.rs"),
	)
	.expect("writing lib.rs");
	fs::write(
		format!("{directory}/Cargo.toml"),
		include_str!("../test/Cargo.toml"),
	)
	.expect("writing Cargo.toml");
}
