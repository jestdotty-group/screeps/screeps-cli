mod js;
use std::{cell::RefCell, panic};
use wasm_bindgen::prelude::wasm_bindgen;

thread_local! {
	pub static DATA: RefCell<Data> = RefCell::new(Default::default());
}
#[derive(Default)]
pub struct Data {
	//RAM
}

#[wasm_bindgen]
pub fn setup() {
	panic::set_hook(Box::new(|info| log!("{}", info.to_string())));
	log!("setup");
}
#[wasm_bindgen]
pub fn tick() {
	log!("ticking");
}
