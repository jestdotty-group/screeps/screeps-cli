const log = (...args) => console.log(`${Game.time} @ ${(new Date()).toISOString()}\t${args.join(' ')}`)
log('loading...');
const minBucket = 100
let wasm
//lets you set memory if you're using RawMemory (at least on the first level)
//can't put on Memory object, they probably overwrite it or something
global.m = new Proxy({}, {
	//use example: m.pixel = true
	set(self, prop, value) {
		RawMemory.set(JSON.stringify({
			...Memory,
			[prop]: value
		})) //would be expensive, probably
		// unofficial API, might be removed later:
		// Memory._parsed = changed
		//https://github.com/screepers/screeps-snippets/blob/8b557a3fcb82cb734fca155b07d5a48622f9da60/src/misc/JavaScript/Memory%20Cache.js#L25
		//however doesn't work with this setup for whatever reason
		if(wasm) wasm.memory_read() //makes sure to update ram
	}
})
module.exports.loop = () => {
	try {
		if(!wasm) {
			if (Game.cpu.bucket < minBucket) return log(`wasm waiting for ${minBucket} bucket, currently ${Game.cpu.bucket}`)
			if ('wasm' in require.cache) delete require.cache['wasm']
			wasm = require('wasm')
			log('loaded wasm')
			wasm.setup()
		}
		wasm.tick()
	}catch (err) {
		console.log(err)
		if(err.stack) console.log(err.stack)
		wasm = null
	}
}
