//! calls javascript functions in rust
pub mod ffi {
	//! wasm usage: https://rustwasm.github.io/docs/wasm-bindgen/introduction.html
	//! serde usage: https://github.com/serde-rs/json#parsing-json-as-strongly-typed-data-structures
	use wasm_bindgen::prelude::*;
	#[wasm_bindgen]
	extern "C" {
		#[wasm_bindgen(js_namespace = console)]
		pub fn log(s: &str);
	}
}
#[macro_export]
macro_rules! log {
	($($arg: tt)*) => {{
		js::ffi::log(&format!($($arg)*))
	}};
}